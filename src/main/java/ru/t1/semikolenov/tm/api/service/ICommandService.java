package ru.t1.semikolenov.tm.api.service;

import ru.t1.semikolenov.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
