package ru.t1.semikolenov.tm.api.repository;

import ru.t1.semikolenov.tm.model.Project;
import ru.t1.semikolenov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    Project remove(Project project);

    Project create(String name);

    Project create(String name, String description);

    Project add(Project project);

    void clear();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    boolean existById(String id);

    Project removeById(String id);

    Project removeByIndex(Integer index);

}
